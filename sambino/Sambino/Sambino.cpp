/*
 * sambino.cpp
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#include <signal.h>
#include <stdlib.h>
#include <exception>      // std::set_terminate
#include <cstdlib>        // std::abort
#include "Sambino.h"

using namespace std;

__attribute__ ((__constructor__))
void libStartup() {
	digitalBegin();
	clockBegin();
}

__attribute__ ((__destructor__))
void libShutdown() {
	clockEnd();
	digitalEnd();
}

void cppTerminationHandler() {
	fprintf(stderr,"\nC++ terminate handler called\n");
	libShutdown();
	abort();
}

void sigTerminationHandler(int signum) {
	fprintf(stderr,"\nSignal %d trapped\n",signum);
	libShutdown();
	exit(1);
}



int main(int argc, char **argv) {

	if (signal (SIGINT, sigTerminationHandler)  == SIG_IGN)
		signal (SIGINT, SIG_IGN);

	if (signal (SIGHUP, sigTerminationHandler)  == SIG_IGN)
		signal (SIGHUP, SIG_IGN);

	if (signal (SIGTERM, sigTerminationHandler) == SIG_IGN)
		signal (SIGTERM, SIG_IGN);

	set_terminate(cppTerminationHandler);

	//libStartup();

	setup();
	while(loop()) {}

	//libShutdown();

    return 0;
}
