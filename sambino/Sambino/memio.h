/*
 * memio.h
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#ifndef MEMIO_H_
#define MEMIO_H_

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <inttypes.h>
#include <fcntl.h>

void* memio_ptr(uint32_t target);

#endif /* MEMIO_H_ */
