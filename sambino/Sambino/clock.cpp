/*
 * clock.cpp
 *
 *  Created on: 28.12.2020
 *      Author: filou
 *
 *      bitbucket
 */

#include "clock.h"
#include "memio.h"

#define TC0_BASE	0xF800C000	// first tc0 register
#define TC0_CNT	 	0x10		// counter, 4 byte units

uint32_t* tc0_cnt;

volatile uint32_t _mstamp_ = 0U;
volatile uint32_t _ustamp_ = 0U;

void delay(int ms) {
	usleep(ms * 1000);
}

void udelay(int us) {
	if ( us > 100 )
		usleep(us);
	else
		for (int i=0; i<4*us; i++) {}
}

//void delayMicroseconds(int us) {
//	if ( us > 100 )
//		usleep(us);
//	else
//		for (int i=0; i<4*us; i++) {}
//}

uint64_t uticks() {
	struct timespec ts;
	unsigned long tick = 0U;
	clock_gettime( CLOCK_REALTIME, &ts );
	tick  = ts.tv_nsec / 1000;
	tick += ts.tv_sec * 1000000;
	return tick;
}

uint32_t mticks() {
	struct timespec ts;
	unsigned long tick = 0U;
	clock_gettime( CLOCK_REALTIME, &ts );
	tick  = ts.tv_nsec / 1000000;
	tick += ts.tv_sec * 1000;
	return tick;
}

time_t millis() {
	return mticks() - _mstamp_;
}

time_t micros() {
	return uticks() - _ustamp_;
}

uint32_t tticks() {
	return *(tc0_cnt);
}

void delayMicroseconds(int us) {
	uint32_t start = *(tc0_cnt);
	uint32_t stop  = start + us;
	while ( *(tc0_cnt) < stop );
}

void clockBegin() {
	_mstamp_ = mticks();
	_ustamp_ = uticks();
	tc0_cnt  = (uint32_t*)memio_ptr(TC0_BASE+TC0_CNT);
}

void clockEnd() {

}


