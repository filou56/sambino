/*
 * clock.h
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#ifndef CLOCK_H_
#define CLOCK_H_

#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <inttypes.h>

void delay(int ms);
time_t millis();
time_t micros();

void delayMicroseconds(int us);

void udelay(int us);

uint64_t uticks();
uint32_t mticks();
uint32_t tticks();

void clockBegin();
void clockEnd();

#endif /* CLOCK_H_ */
