/*
 * digital.cpp
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#include "../Sambino/digital.h"

/********** pin mapping ICS port *************************

				3v3			1	2		GND
				3v3			3	4		GND
		D2		PD6			5	6		PD7			D3
		D1		PD5			7	8		PD4			D0
				GND			9	10		PC24		D16
				GND			11	12		PC22		D17
				GND			13	14		PC23		D18
				GND			15	16		PC21		D10*
				GND			17	18		PC9			D19
		D4		PC10		19	20		PC11		D5
		D6		PC12		21	22		PC13		D7
		D8		PC14		23	24		PC15		D9
		D14		PC16		25	26		PC17		D15
		D11*	PC18		27	28		PC19		D12*
		D13*	PC20		29	30		GND


				PC18	FLEXCOM3_IO2	SCK		SPCK
				PC19	FLEXCOM3_IO1	RXD		MISO	TWCK
				PC20	FLEXCOM3_IO0	TXD		MOSI	TWD
				PC21	FLEXCOM3_IO3	CTS		CS0
				PC22	FLEXCOM3_IO4	RT		CS1
 */

typedef struct {
	uint8_t		port;
	uint8_t		bit;
	uint32_t	mask;
} PIN_T;

#define GPIO_PINS 20

const PIN_T pio_register[GPIO_PINS] = {
//		port index * 16 registers per port, 4 byte units
// 		port, 		bit, 	mask
		{3 * 0x10,	4,		1 <<  4 },		// DO
		{3 * 0x10,	5,		1 <<  5 },		// D1
		{3 * 0x10,	6,		1 <<  6 },		// D2
		{3 * 0x10,	7,		1 <<  7 },		// D3
		{2 * 0x10,	10,		1 << 10 },		// D4
		{2 * 0x10,	11,		1 << 11 },		// D5
		{2 * 0x10,	12,		1 << 12 },		// D6
		{2 * 0x10,	13,		1 << 13 },		// D7
		{2 * 0x10,	14,		1 << 14 },		// D8
		{2 * 0x10,	15,		1 << 15 },		// D9
		{2 * 0x10,	21,		1 << 21 },		// D10
		{2 * 0x10,	18,		1 << 18 },		// D11
		{2 * 0x10,	19,		1 << 19 },		// D12
		{2 * 0x10,	20,		1 << 20 },		// D13
		{2 * 0x10,	16,		1 << 16 },		// D14
		{2 * 0x10,	17,		1 << 17 },		// D15
		{2 * 0x10,	24,		1 << 24 },		// D16
		{2 * 0x10,	22,		1 << 22 },		// D17
		{2 * 0x10,	23,		1 << 23 },		// D18
		{2 * 0x10,	9,		1 <<  9 },		// D19
};

#define PIO_BASE	0xFC038000	// first pio register
#define PIO_MSKR	0x00		// register number, 4 byte units
#define PIO_CFGR	0x01
#define PIO_PDSR	0x02
#define PIO_SODR 	0x04
#define PIO_CODR 	0x05

uint32_t* pio_base;

void pinMode(int pin, int mode) {
	*(pio_base+PIO_MSKR+pio_register[pin].port) = pio_register[pin].mask;
	*(pio_base+PIO_CFGR+pio_register[pin].port) = mode;
}

void digitalWrite(int pin, int val) {
	if (val)
		*(pio_base+PIO_SODR+pio_register[pin].port) = pio_register[pin].mask;
	else
		*(pio_base+PIO_CODR+pio_register[pin].port) = pio_register[pin].mask;
}

int  digitalRead(int pin) {
	return (*(pio_base+PIO_PDSR+pio_register[pin].port) & pio_register[pin].mask) != 0;
}

void digitalBegin() {
	// init base pointer on page boundery
	pio_base	=	(uint32_t*)memio_ptr(PIO_BASE);

	// default, all inputs
	for (int i=0; i < GPIO_PINS; i++) {
		pinMode(i,INPUT);
	}
}

void digitalEnd() {

}

