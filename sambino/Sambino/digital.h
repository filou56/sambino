/*
 * digital.h
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#ifndef DIGITAL_H_
#define DIGITAL_H_

#include "../Sambino/memio.h"

#define INPUT 	0x00000000
#define OUTPUT 	0x00000100

void pinMode(int pin, int mode);
void digitalWrite(int pin, int val);
int  digitalRead(int pin);

void digitalBegin();
void digitalEnd();

#endif /* DIGITAL_H_ */
