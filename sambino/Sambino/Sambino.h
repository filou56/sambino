/*
 * sambino.h
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#ifndef SAMBINO_H_
#define SAMBINO_H_

#include "../Sambino/clock.h"
#include "../Sambino/digital.h"

#define HIGH 	1
#define LOW 	0

#define LOOP_DONE 0
#define LOOP_RUN  1

#define true 	0x1
#define false 	0x0

#define min(a,b) ((a)<(b)?(a):(b))
#define max(a,b) ((a)>(b)?(a):(b))
#define abs(x) ((x)>0?(x):-(x))


void setup(void);
int  loop(void);

#endif /* SAMBINO_H_ */
