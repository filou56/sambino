/*
 * memio.cpp
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

#include "../Sambino/memio.h"

void* memio_ptr(uint32_t target) {
	uint8_t*	base;
	uint32_t	offset;
	uint32_t	page_size;
	uint32_t	mapped_size;

	page_size = mapped_size = getpagesize();
	int fd 	  = open("/dev/mem", O_RDWR);

	offset = (unsigned)target & (page_size - 1);
	base   = (uint8_t*)mmap(NULL,mapped_size,(PROT_READ | PROT_WRITE),MAP_SHARED,fd,
			target & ~(off_t)(page_size - 1));
	close(fd);

	if ( base == MAP_FAILED ) {
		return NULL;
	} else {
		return (void*)(base + offset);
	}

}


