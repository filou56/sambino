/*
 * Wire.h
 *
 *  Created on: 08.01.2021
 *      Author: filou
 */

#ifndef INCLUDE_WIRE_H_
#define INCLUDE_WIRE_H_

#include "../lib/Wire/SlowSoftWire.h"

#define TwoWire SlowSoftWire

#endif /* INCLUDE_WIRE_H_ */
