/*
 * Config.h
 *
 *  Created on: 08.01.2021
 *      Author: filou
 */

#ifndef INCLUDE_CONFIG_H_
#define INCLUDE_CONFIG_H_

#define WIRE_SDA 1
#define WIRE_SCL 2

#endif /* INCLUDE_CONFIG_H_ */
