/*
 * Ardino.h
 *
 *  Created on: 08.01.2021
 *      Author: filou
 */

#ifndef INCLUDE_ARDUINO_H_
#define INCLUDE_ARDUINO_H_

#include "../Sambino/Sambino.h"

#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#define bitSet(value, bit) ((value) |= (1UL << (bit)))
#define bitClear(value, bit) ((value) &= ~(1UL << (bit)))
#define bitWrite(value, bit, bitvalue) (bitvalue ? bitSet(value, bit) : bitClear(value, bit))

#define lowByte(w) ((w) & 0xff)
#define highByte(w) ((w) >> 8)

typedef  uint8_t byte;

#endif /* INCLUDE_ARDUINO_H_ */
