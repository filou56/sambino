run_gpio.cpp
============

$Bit Zähler:

D1 / PD5	/ Pin 7
D0 / PD4	/ Pin 9
D2 / PD6	/ Pin 5	
D3 / PD7	/ Pin 6

run_bounce.cpp
==============

LED Anode 470 Ohm nach 3.3V				/ Pin 1
LED Kathode an Output 		D14 / PC16	/ Pin 25
Pullup 220 kOhm nach 3.3 V				/ Pin 1
Taster an Pullup  			D8  / PC14	/ Pin23
Taster nach GND							/ Pin2


run_dht22.cpp
=============

Vcc	= 3.3V			/ Pin 1
Out = D8 	/ PC14 	/ Pin 23
Gnd = GND 			/ Pin 2

Arduino Library von https://github.com/RobTillaart/Arduino
Backport: AVR Fast-Port-IO zurück zu standart digitalRead.

