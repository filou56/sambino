/*
 * run.cpp
 *
 *  Created on: 28.12.2020
 *      Author: filou
 */

//#define RUN_GPIO
//#define RUN_BOUNCE
//#define RUN_DHT22
//#define RUN_SHT21
//#define RUN_MCP
#define RUN_MCP9600

#ifdef RUN_MCP9600
#include <Arduino.h>
#include <Wire.h>

#include "lib/MCP9600/MCP9600.h"
MCP9600 tempSensor;

void setup(){
    Wire.begin();
    // Uses the default address (0x60) for SparkFun Thermocouple Amplifier
    tempSensor.begin();
    //check if the sensor is connected
    if(tempSensor.isConnected()){
        fprintf(stderr,"Device will acknowledge!\n");
    }
    else {
        fprintf(stderr,"Device did not acknowledge! Freezing.\n");
        while(1); //hang forever
    }

    //check if the Device ID is correct
    if(tempSensor.checkDeviceID()){
        fprintf(stderr,"Device ID is correct!\n");
    }
    else {
        fprintf(stderr,"Device ID is not correct! Freezing.\n");
        while(1);
    }
}

int loop(){ //print the thermocouple, ambient and delta temperatures every 200ms if available
    if(tempSensor.available()){
        fprintf(stderr,"Thermocouple:  %4.2f °C\n",tempSensor.getThermocoupleTemp());
        fprintf(stderr,"Ambient:       %4.2f °C\n",tempSensor.getAmbientTemp());
        fprintf(stderr,"Delta:         %4.2f °C\n\n",tempSensor.getTempDelta());
        delay(20); //don't hammer too hard on the I2C bus
    }

    return LOOP_RUN;
}
#endif

#ifdef RUN_MCP
#include <Arduino.h>
#include <Wire.h>

#define I2C_TRIGGER

void setup() {
	Wire.begin();
#ifdef I2C_TRIGGER
	pinMode(14,OUTPUT);
#endif
}

float readTemperature(uint32_t r) {
	byte b1,b2;

#ifdef I2C_TRIGGER
	digitalWrite(14,HIGH);
	delay(1);
#endif

	Wire.requestFrom(0x60, 2,r<<8, 2, true);
	while(Wire.available() < 2) {
		delay(1);
	}

	b1 = Wire.read();
	b2 = Wire.read();

#ifdef I2C_TRIGGER
	digitalWrite(14,LOW);
#endif

	if ( b1 > 128 ) {
		return (b1 & 0x7F)*16.0 + b2/16.0 - 2048.0 ;
	} else {
		return  b1*16.0 + b2/16.0 ;
	}

}

int loop() {

	fprintf(stderr,"R0 = %4.2f\n",readTemperature(0));
	fprintf(stderr,"R1 = %4.2f\n",readTemperature(1));
	fprintf(stderr,"R2 = %4.2f\n",readTemperature(2));

	//fprintf(stderr,"TC = %u\n",tticks());

	delay(1000);

	return LOOP_RUN;
}

#endif

#ifdef RUN_SHT21

#include <Arduino.h>
#include "lib/SHT21/SHT21.h"

SHT21 sht = SHT21();

void setup() {
  sht.begin();
}

int loop() {
	fprintf(stderr,"Humidity(%RH):  %4.2f\n",sht.getHumidity());
	fprintf(stderr,"Temperature(C): %4.2f\n",sht.getTemperature());
	delay(100);

	return LOOP_RUN;
}

#endif

#ifdef RUN_DHT22

#include "lib/DHT22/dht.h"

dht DHT;

#define DHT22_PIN 8

struct
{
    uint32_t total;
    uint32_t ok;
    uint32_t crc_error;
    uint32_t time_out;
    uint32_t connect;
    uint32_t ack_l;
    uint32_t ack_h;
    uint32_t unknown;
} dht_stat = { 0,0,0,0,0,0,0,0};

void setup()
{
}

int loop()
{
    // READ DATA

    uint32_t start = micros();
    int chk = DHT.read22(DHT22_PIN);
    uint32_t stop = micros();

    if ( chk == DHTLIB_OK ) {
    	fprintf(stderr,"Humidity:    %3.1f %%\n",DHT.humidity);
    	fprintf(stderr,"Temperature: %3.1f °C\n",DHT.temperature, 1);
    	fprintf(stderr,"Time:        %d us\n\n",stop - start);
    }

    delay(2000);

    return LOOP_RUN;
}

#endif

#ifdef RUN_GPIO

#include <Arduino.h>
#include "Sambino/gpio.h"

uint32_t ts;

gpio_t PD4, PD5, PD6, PD7;

void setup() {
	// Open GPIO 100..103 with output direction PIN 3 * 32 + 4..
	gpio_open(&PD4, 100, GPIO_DIR_OUT);
	gpio_open(&PD5, 101, GPIO_DIR_OUT);
	gpio_open(&PD6, 102, GPIO_DIR_OUT);
	gpio_open(&PD7, 103, GPIO_DIR_OUT);

	pinMode(0,OUTPUT);
	pinMode(1,OUTPUT);
	pinMode(2,OUTPUT);
	pinMode(3,OUTPUT);

	ts = millis();
}

int loop() {
	static int count;

	uint64_t tg, td, ts;

	ts = uticks();
	for (int i=0; i<1000; i++) {
		gpio_write(&PD4, count & 0x01);
		gpio_write(&PD5, count & 0x02);
		gpio_write(&PD6, count & 0x04);
		gpio_write(&PD7, count & 0x08);

		count++;
	}
	tg = uticks() - ts;

	ts = uticks();
	for (int i=0; i<1000; i++) {
		digitalWrite(0,count & 0x01);
		digitalWrite(1,count & 0x02);
		digitalWrite(2,count & 0x04);
		digitalWrite(3,count & 0x08);

		count++;
	}
	td = uticks() - ts;

	fprintf(stderr,"tg = %6d us\n",tg);
	fprintf(stderr,"td = %6d us\n",td);
	fprintf(stderr,"--------------\n",td);

	delay(1);
	//
	//	if ( (millis() - ts) > 10000 )
		//		return LOOP_DONE;
	//	else
	return LOOP_RUN;
}

#endif

#ifdef RUN_BOUNCE

#include <Arduino.h>
#include "lib/Bounce/Bounce.h"

#define BUTTON 	8
#define LED 	14

// Instantiate a Bounce object with a 50 millisecond debounce time
Bounce bouncer = Bounce(BUTTON,50);

int count = 0;

void setup() {
	pinMode(BUTTON,INPUT);
	pinMode(LED,OUTPUT);
}

int loop() {
	// Update the debouncer
	bouncer.update();

	// Get the update value
	int value = bouncer.read();

	// Turn on or off the LED
	if ( value == HIGH) {
		digitalWrite(LED, HIGH );
	} else {
		digitalWrite(LED, LOW );
	}

	if ( bouncer.fallingEdge() ) {
		count++;
		fprintf(stderr,"Count: = %d\n",count);
	}

	return LOOP_RUN;
}

#endif
